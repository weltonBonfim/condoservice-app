package com.example.weltonlima.condoservices.entidades;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

        @SerializedName("tipo")
        @Expose
        private String tipo;
        @SerializedName("andar")
        @Expose
        private String andar;
        @SerializedName("numero")
        @Expose
        private String numero;
        @SerializedName("auth")
        @Expose
        private Boolean auth;
        @SerializedName("primeiro_acesso")
        @Expose
        private Boolean primeiroAcesso;
        @SerializedName("bloco")
        @Expose
        private String bloco;
        @SerializedName("razao_social")
        @Expose
        private String razaoSocial;
        @SerializedName("cpf")
        @Expose
        private String cpf;
        @SerializedName("nome")
        @Expose
        private String nome;
        @SerializedName("cnpj")
        @Expose
        private String cnpj;
        @SerializedName("login")
        @Expose
        private String login;

        public String getTipo() {
            return tipo;
        }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAuth() {
        return auth;
    }

    public void setAuth(Boolean auth) {
        this.auth = auth;
    }

    public Boolean getPrimeiroAcesso() {
        return primeiroAcesso;
    }

    public void setPrimeiroAcesso(Boolean primeiroAcesso) {
        this.primeiroAcesso = primeiroAcesso;
    }

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
