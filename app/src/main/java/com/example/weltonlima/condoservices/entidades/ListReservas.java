package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListReservas {

    @SerializedName("data_ini_reserva")
    @Expose
    private String dataIniReserva;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("data_fim_reserva")
    @Expose
    private String dataFimReserva;

    public String getDataIniReserva() {
        return dataIniReserva;
    }

    public void setDataIniReserva(String dataIniReserva) {
        this.dataIniReserva = dataIniReserva;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataFimReserva() {
        return dataFimReserva;
    }

    public void setDataFimReserva(String dataFimReserva) {
        this.dataFimReserva = dataFimReserva;
    }
}


