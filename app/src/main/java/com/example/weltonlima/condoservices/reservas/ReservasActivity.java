package com.example.weltonlima.condoservices.reservas;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.Spinner;
import android.widget.TextView;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.AreaComum;
import com.example.weltonlima.condoservices.entidades.ListAreaComum;
import com.example.weltonlima.condoservices.entidades.ListReservas;
import com.example.weltonlima.condoservices.entidades.ReservaResponse;
import com.example.weltonlima.condoservices.entidades.Reservas;
import com.example.weltonlima.condoservices.interfaces.IServices;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservasActivity extends AppCompatActivity {
    private EditText date;
    private Calendar myCalendar = Calendar.getInstance();
    final Locale myLocale = new Locale("pt", "BR");
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Spinner dropdown;
    private CalendarView calendarView;
    final private List<Integer> ids = new ArrayList<Integer>();
    private int idEspaco;
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private List<ListReservas> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        dropdown = findViewById(R.id.spinner1);

        getListAreaComum();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        date = (EditText)findViewById(R.id.date_reserva);

        final DatePickerDialog dialog = new DatePickerDialog(ReservasActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        },
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             dialog.show();
            }
        });


        final TextView espaco = (TextView) findViewById(R.id.espaco);
        espaco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropdown.setVisibility(View.VISIBLE);
                dropdown.performClick();
            }
        });

        espaco.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    dropdown.setVisibility(View.VISIBLE);
                    dropdown.performClick();
                }
                else{
                    dropdown.setVisibility(View.GONE);
                }
            }
        });

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                espaco.setText(dropdown.getSelectedItem().toString());
                idEspaco = ids.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                espaco.setText("");
            }
        });

        Button btn_reserva = (Button) findViewById(R.id.btn_fazer_reserva);

        btn_reserva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserva();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycleViewReservas);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerAdapter(list);
        recyclerView.setAdapter(recyclerAdapter);

        getListaReservas();

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, myLocale);
        date.setText(sdf.format(myCalendar.getTime()));
    }

    private void getListAreaComum(){

            sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

            retrofit2.Retrofit retrofit = new Retrofit().create();

            IServices services = retrofit.create(IServices.class);

            Call<AreaComum> areaComum = services.areaComum(sharedPreferences.getString("cnpj", ""));

        areaComum.enqueue(new Callback<AreaComum>() {
                @Override
                public void onResponse(Call<AreaComum> call, Response<AreaComum> response) {

                    AreaComum res = response.body();

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(ReservasActivity.this, android.R.layout.simple_spinner_dropdown_item);

                    for(ListAreaComum area : res.getLista()){
                        adapter.addAll(area.getNome());
                        ids.add(area.getIdArea());
                    }

                    dropdown.setAdapter(adapter);

                }
                @Override
                public void onFailure(Call<AreaComum> call, Throwable t) {

                    new AlertDialog.Builder(ReservasActivity.this)
                            .setTitle("Atenção!")
                            .setMessage(t.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }
            });
        }

        private void reserva() {
            progressDialog = new ProgressDialog(ReservasActivity.this);
            progressDialog.setMessage("Processando...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            retrofit2.Retrofit retrofit = new Retrofit().create();

            IServices services = retrofit.create(IServices.class);

            sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

            String[] dateFormat = date.getText().toString().split("/");

            JsonObject json = new JsonObject();
            json.addProperty("data_ini_reserva", dateFormat[2] + "-" + dateFormat[1] + "-" + dateFormat[0]);
            json.addProperty("data_fim_reserva", dateFormat[2] + "-" + dateFormat[1] + "-" + dateFormat[0]);
            json.addProperty("id_area", idEspaco);
            json.addProperty("login_usuario", sharedPreferences.getString("login", ""));
            json.addProperty("cnpj_condominio", sharedPreferences.getString("cnpj", ""));

            Call<ReservaResponse> reserva = services.reservar(json);

            reserva.enqueue(new Callback<ReservaResponse>() {
                @Override
                public void onResponse(Call<ReservaResponse> call, Response<ReservaResponse> response) {

                    ReservaResponse reposResponse = response.body();

                    progressDialog.dismiss();

                    if(reposResponse.getStatus()){
                        new AlertDialog.Builder(ReservasActivity.this)
                                .setTitle("")
                                .setMessage(reposResponse.getMessage())
                                .setCancelable(false)
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .show();

                        getListaReservas();
                    }else {
                        new AlertDialog.Builder(ReservasActivity.this)
                                .setTitle("")
                                .setMessage(reposResponse.getMessage())
                                .setCancelable(false)
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .show();
                    }

                }
                @Override
                public void onFailure(Call<ReservaResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(ReservasActivity.this)
                            .setTitle("Atenção!")
                            .setMessage(t.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }
            });
        }


        private void getListaReservas() {
            progressDialog = new ProgressDialog(ReservasActivity.this);
            progressDialog.setMessage("Processando...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            retrofit2.Retrofit retrofit = new Retrofit().create();

            IServices services = retrofit.create(IServices.class);

            sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

            Call<Reservas> reserva = services.listaReserva(sharedPreferences.getString("cnpj", ""));

            reserva.enqueue(new Callback<Reservas>() {
                @Override
                public void onResponse(Call<Reservas> call, Response<Reservas> response) {

                    progressDialog.dismiss();

                    Reservas res = response.body();

                    list.clear();
                    list.addAll(res.getLista());
                    recyclerAdapter.notifyDataSetChanged();

                }
                @Override
                public void onFailure(Call<Reservas> call, Throwable t) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(ReservasActivity.this)
                            .setTitle("Atenção!")
                            .setMessage(t.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }
            });
        }
}
