package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListMessages {

    @SerializedName("nome_usuario")
    @Expose
    private String nomeUsuario;
    @SerializedName("cnpj_condominio")
    @Expose
    private String cnpjCondominio;
    @SerializedName("data_ocorrencia")
    @Expose
    private String dataOcorrencia;
    @SerializedName("id_ocorrencia")
    @Expose
    private Integer idOcorrencia;
    @SerializedName("titulo")
    @Expose
    private String titulo;
    @SerializedName("descricao")
    @Expose
    private String descricao;

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCnpjCondominio() {
        return cnpjCondominio;
    }

    public void setCnpjCondominio(String cnpjCondominio) {
        this.cnpjCondominio = cnpjCondominio;
    }

    public String getDataOcorrencia() {
        return dataOcorrencia;
    }

    public void setDataOcorrencia(String dataOcorrencia) {
        this.dataOcorrencia = dataOcorrencia;
    }

    public Integer getIdOcorrencia() {
        return idOcorrencia;
    }

    public void setIdOcorrencia(Integer idOcorrencia) {
        this.idOcorrencia = idOcorrencia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
