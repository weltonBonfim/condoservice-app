package com.example.weltonlima.condoservices.mensagens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.login.MainActivity;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.example.weltonlima.condoservices.entidades.ListMessages;
import com.example.weltonlima.condoservices.entidades.Messages;
import com.example.weltonlima.condoservices.interfaces.IServices;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MensagensActivity  extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private List<ListMessages> list = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private LinearLayout empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensagens);

        empty = (LinearLayout) findViewById(R.id.empty);
        empty.setVisibility(View.INVISIBLE);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerAdapter(list);
        recyclerView.setAdapter(recyclerAdapter);

        getListMessages();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void getListMessages() {
        progressDialog = new ProgressDialog(MensagensActivity.this);
        progressDialog.setMessage("Processando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

        retrofit2.Retrofit retrofit = new Retrofit().create();

        IServices services = retrofit.create(IServices.class);

        Call<Messages> messages = services.messages(sharedPreferences.getString("cnpj", ""));

        messages.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(Call<Messages> call, Response<Messages> response) {

                Messages res = response.body();

                progressDialog.dismiss();

                if(res.getLista().size() > 0){
                    list.clear();
                    list.addAll(res.getLista());
                    recyclerAdapter.notifyDataSetChanged();
                }else {
                    empty.setVisibility(View.VISIBLE);
                    recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }
             }
            @Override
            public void onFailure(Call<Messages> call, Throwable t) {

                progressDialog.dismiss();

                new AlertDialog.Builder(MensagensActivity.this)
                        .setTitle("Atenção!")
                        .setMessage(t.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();

                empty.setVisibility(View.VISIBLE);
                recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            }
        });
    }
}
