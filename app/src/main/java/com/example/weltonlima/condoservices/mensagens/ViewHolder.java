package com.example.weltonlima.condoservices.mensagens;

import android.view.View;
import android.widget.TextView;
import com.example.weltonlima.condoservices.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView titulo;
    public TextView descricao;
    public TextView autor;
    public TextView data_post;
    public TextView readmore;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        this.titulo = (TextView) itemView.findViewById(R.id.titulo);
        this.descricao = (TextView) itemView.findViewById(R.id.descricao);
        this.autor = (TextView) itemView.findViewById(R.id.autor);
        this.data_post = (TextView) itemView.findViewById(R.id.data);
        this.readmore = (TextView) itemView.findViewById(R.id.readmoreMessage);

    }

}
