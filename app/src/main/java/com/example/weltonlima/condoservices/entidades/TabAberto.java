package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TabAberto {
    @SerializedName("lista")
    @Expose
    private List<ListTabAberto> lista = null;

    public List<ListTabAberto> getLista() {
        return lista;
    }

    public void setLista(List<ListTabAberto> lista) {
        this.lista = lista;
    }
}
