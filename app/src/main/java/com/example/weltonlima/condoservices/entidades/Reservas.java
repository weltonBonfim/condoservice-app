package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Reservas {
    @SerializedName("lista")
    @Expose
    private List<ListReservas> lista = null;

    public List<ListReservas> getLista() {
        return lista;
    }

    public void setLista(List<ListReservas> lista) {
        this.lista = lista;
    }
}
