package com.example.weltonlima.condoservices.ocorrencias;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.Messages;
import com.example.weltonlima.condoservices.interfaces.IServices;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OcorrenciasActivity  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocorrencias);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        TabLayout tabLayout = findViewById(R.id.tabs);
        TabItem tabEncerrado = findViewById(R.id.aberto);
        TabItem tabAberto = findViewById(R.id.encerrado);
        ViewPager viewPager = findViewById(R.id.viewPager);

        TabsAdapter tabs = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabs);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}