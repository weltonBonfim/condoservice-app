package com.example.weltonlima.condoservices.ocorrencias;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListTabAberto;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterTabAberto extends RecyclerView.Adapter<ViewHolderTabAberto> {

    private List<ListTabAberto> list;
    private int cont = 0;
    private int tamanhoText;

    public RecyclerAdapterTabAberto(List<ListTabAberto> list){this.list = list;}

    @NonNull
    @Override
    public ViewHolderTabAberto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ocorrencia_aberto, parent, false);

        return new ViewHolderTabAberto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderTabAberto holder, int position) {
        ListTabAberto listTabAberto = list.get(position);

        holder.data_aberto.setText("");
        holder.descricao_aberto.setText(listTabAberto.getMensagemIn());
        holder.resposta_aberto.setText(listTabAberto.getMensagemOut());
        holder.status_aberto.setText(listTabAberto.getStatusOcorrencia());

        tamanhoText = holder.resposta_aberto.length();
        holder.readmore_aberto.setPaintFlags(holder.readmore_aberto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if(!(tamanhoText > 100)) {
            holder.readmore_aberto.setVisibility(View.INVISIBLE);
        }else {
            holder.readmore_aberto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cont == 0) {
                        holder.readmore_aberto.setText("Leia menos");
                        holder.resposta_aberto.setMaxLines(Integer.MAX_VALUE);
                        cont++;
                    } else {
                        holder.readmore_aberto.setText("Leia mais");
                        holder.resposta_aberto.setMaxLines(4);
                        cont = 0;
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
