package com.example.weltonlima.condoservices.reservas;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListReservas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<ListReservas> list;
    final Locale myLocale = new Locale("pt", "BR");

    public RecyclerAdapter(List<ListReservas> list){
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_reservas, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        ListReservas listReservas = list.get(position);

        holder.dia_reserva.setText(listReservas.getDataIniReserva().split("-")[2]);
        holder.mes_reserva.setText(listReservas.getDataIniReserva().split("-")[1]);
        holder.nome_reserva.setText(listReservas.getNome());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
