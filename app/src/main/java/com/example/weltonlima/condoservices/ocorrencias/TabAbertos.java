package com.example.weltonlima.condoservices.ocorrencias;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListTabAberto;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.example.weltonlima.condoservices.entidades.TabAberto;
import com.example.weltonlima.condoservices.interfaces.IServices;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabAbertos extends Fragment {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private List<ListTabAberto> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerAdapterTabAberto recyclerAdapter;
    private LinearLayout empty;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_aberto, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewTabAberto);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerAdapter = new RecyclerAdapterTabAberto(list);
        recyclerView.setAdapter(recyclerAdapter);

        empty = (LinearLayout) rootView.findViewById(R.id.emptyTabAberto);
        empty.setVisibility(View.INVISIBLE);

        getOcorrencias();

        return rootView;
    }

    public void getOcorrencias() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Processando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        sharedPreferences = this.getActivity().getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

        retrofit2.Retrofit retrofit = new Retrofit().create();

        IServices services = retrofit.create(IServices.class);

        Call<TabAberto> ocorrencias = services.ocorrenciasAberto(
                sharedPreferences.getString("cnpj", ""),
                sharedPreferences.getString("tipo", ""),
                sharedPreferences.getString("login", ""),
                "Em Aberto"
        );

        ocorrencias.enqueue(new Callback<TabAberto>() {
            @Override
            public void onResponse(Call<TabAberto> call, Response<TabAberto> response) {

                progressDialog.dismiss();

                TabAberto res = response.body();

                if(res.getLista().size() > 0){
                    list.clear();
                    list.addAll(res.getLista());
                    recyclerAdapter.notifyDataSetChanged();
                }else {
                    empty.setVisibility(View.VISIBLE);
                    recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }

            }
            @Override
            public void onFailure(Call<TabAberto> call, Throwable t) {

                progressDialog.dismiss();

                new AlertDialog.Builder(getActivity())
                        .setTitle("Atenção!")
                        .setMessage(t.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();

                empty.setVisibility(View.VISIBLE);
                recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            }
        });
    }
}
