package com.example.weltonlima.condoservices.ocorrencias;

import android.view.View;
import android.widget.TextView;

import com.example.weltonlima.condoservices.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderTabAberto extends RecyclerView.ViewHolder{

    public TextView data_aberto;
    public TextView descricao_aberto;
    public TextView resposta_aberto;
    public TextView readmore_aberto;
    public TextView status_aberto;


    public ViewHolderTabAberto(@NonNull View itemView) {
        super(itemView);
        this.data_aberto = (TextView) itemView.findViewById(R.id.data_ocorrencia_aberto);
        this.descricao_aberto = (TextView) itemView.findViewById(R.id.descricao_aberto);
        this.resposta_aberto = (TextView) itemView.findViewById(R.id.resposta_aberto);
        this.readmore_aberto = (TextView) itemView.findViewById(R.id.readmore_aberto);
        this.status_aberto = (TextView) itemView.findViewById(R.id.status_aberto);

    }

}
