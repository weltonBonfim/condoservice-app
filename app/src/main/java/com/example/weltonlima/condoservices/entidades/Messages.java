package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Messages {

    @SerializedName("lista")
    @Expose
    private List<ListMessages> lista = null;

    public List<ListMessages> getLista() {
        return lista;
    }

    public void setLista(List<ListMessages> lista) {
        this.lista = lista;
    }
}
