package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TabEncerrado {

    @SerializedName("lista")
    @Expose
    private List<ListTabEncerrado> lista = null;

    public List<ListTabEncerrado> getLista() {
        return lista;
    }

    public void setLista(List<ListTabEncerrado> lista) {
        this.lista = lista;
    }
}
