package com.example.weltonlima.condoservices.mensagens;

import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListMessages;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<ListMessages> list;
    private int cont = 0;
    private int tamanhoText;

    public RecyclerAdapter(List<ListMessages> list){
     this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        ListMessages listMessages = list.get(position);
        holder.titulo.setText(listMessages.getTitulo());
        holder.descricao.setText(Html.fromHtml(listMessages.getDescricao()));
        holder.autor.setText(listMessages.getNomeUsuario());
        holder.data_post.setText(listMessages.getDataOcorrencia());
        tamanhoText = holder.descricao.length();
        holder.readmore.setPaintFlags(holder.readmore.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if(!(tamanhoText > 100)) {
            holder.readmore.setVisibility(View.INVISIBLE);
        }else {
            holder.readmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cont == 0) {
                        holder.readmore.setText("Leia menos");
                        holder.descricao.setMaxLines(Integer.MAX_VALUE);
                        cont++;
                    } else {
                        holder.readmore.setText("Leia mais");
                        holder.descricao.setMaxLines(4);
                        cont = 0;
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
