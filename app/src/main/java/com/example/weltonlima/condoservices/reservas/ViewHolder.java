package com.example.weltonlima.condoservices.reservas;

import android.view.View;
import android.widget.TextView;

import com.example.weltonlima.condoservices.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView mes_reserva;
    public TextView dia_reserva;
    public TextView nome_reserva;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        this.mes_reserva = (TextView) itemView.findViewById(R.id.mes_reserva);
        this.dia_reserva = (TextView) itemView.findViewById(R.id.dia_reserva);
        this.nome_reserva = (TextView) itemView.findViewById(R.id.nome_reserva);
    }

}
