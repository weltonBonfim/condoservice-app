package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListAreaComum {

    @SerializedName("cnpj_condominio")
    @Expose
    private String cnpjCondominio;
    @SerializedName("limite_horario")
    @Expose
    private String limiteHorario;
    @SerializedName("limite_pessoas")
    @Expose
    private Integer limitePessoas;
    @SerializedName("valor")
    @Expose
    private Integer valor;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("id_area")
    @Expose
    private Integer idArea;
    @SerializedName("detalhes")
    @Expose
    private String detalhes;
    @SerializedName("descricao")
    @Expose
    private String descricao;

    public String getCnpjCondominio() {
        return cnpjCondominio;
    }

    public void setCnpjCondominio(String cnpjCondominio) {
        this.cnpjCondominio = cnpjCondominio;
    }

    public String getLimiteHorario() {
        return limiteHorario;
    }

    public void setLimiteHorario(String limiteHorario) {
        this.limiteHorario = limiteHorario;
    }

    public Integer getLimitePessoas() {
        return limitePessoas;
    }

    public void setLimitePessoas(Integer limitePessoas) {
        this.limitePessoas = limitePessoas;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
