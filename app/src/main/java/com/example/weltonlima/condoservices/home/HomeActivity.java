package com.example.weltonlima.condoservices.home;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.weltonlima.condoservices.login.MainActivity;
import com.example.weltonlima.condoservices.ocorrencias.OcorrenciasActivity;
import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.reservas.ReservasActivity;
import com.example.weltonlima.condoservices.mensagens.MensagensActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public TextView nome;
    public TextView condominio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();

        setNavigationViewListner();

        sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

    }

    private void setNavigationViewListner() {
        sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(HomeActivity.this);

        View header = navigationView.getHeaderView(0);
        TextView nomeSide = (TextView)header.findViewById(R.id.nome_side_menu);
        TextView razaoSocial = (TextView)header.findViewById(R.id.razao_social_side_menu);

        nomeSide.setText(sharedPreferences.getString("nome", ""));
        razaoSocial.setText(sharedPreferences.getString("razao_social", ""));

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.side_ocorrencias: {
                startActivity(new Intent(HomeActivity.this, OcorrenciasActivity.class));
                break;
            }
            case R.id.side_reservas: {
                startActivity(new Intent(HomeActivity.this, ReservasActivity.class));
                break;
            }
            case R.id.side_messages: {
                startActivity(new Intent(HomeActivity.this, MensagensActivity.class));
                break;
            }
            case R.id.side_exit: {
                new AlertDialog.Builder(HomeActivity.this)
                        .setTitle("Atenção!")
                        .setMessage("Deseja realmente sair?")
                        .setCancelable(false)
                        .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);
                                editor = sharedPreferences.edit();
                                editor.remove("auth");
                                editor.commit();
                                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                                finish();
                            }
                        })
                        .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();

                break;
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
