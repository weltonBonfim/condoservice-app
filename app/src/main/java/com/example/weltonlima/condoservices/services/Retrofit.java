package com.example.weltonlima.condoservices.services;

import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {

    final String URL_BASE = "http://192.168.1.20:8080/rest-api/rest/";

    public retrofit2.Retrofit create() {
        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
