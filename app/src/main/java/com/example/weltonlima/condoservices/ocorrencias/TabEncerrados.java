package com.example.weltonlima.condoservices.ocorrencias;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListTabEncerrado;
import com.example.weltonlima.condoservices.entidades.Ocorrencias;
import com.example.weltonlima.condoservices.entidades.TabEncerrado;
import com.example.weltonlima.condoservices.login.MainActivity;
import com.example.weltonlima.condoservices.mensagens.MensagensActivity;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.example.weltonlima.condoservices.interfaces.IServices;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabEncerrados extends Fragment {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private List<ListTabEncerrado> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerAdapterTabEncerrado recyclerAdapter;
    private LinearLayout empty;
    private ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View rootView = inflater.inflate(R.layout.tab_encerrado, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewTabEncerrado);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        empty = (LinearLayout) rootView.findViewById(R.id.emptyTabEncerrado);
        empty.setVisibility(View.INVISIBLE);

        recyclerAdapter = new RecyclerAdapterTabEncerrado(list);
        recyclerView.setAdapter(recyclerAdapter);

        getOcorrencias();

        return rootView;
    }

    public void getOcorrencias() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Processando...");
        progressDialog.setCancelable(false);

        progressDialog.show();

        sharedPreferences = this.getActivity().getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

        retrofit2.Retrofit retrofit = new Retrofit().create();

        IServices services = retrofit.create(IServices.class);

        Call<TabEncerrado> ocorrencias = services.ocorrenciasEncerrado(
                sharedPreferences.getString("cnpj", ""),
                sharedPreferences.getString("tipo", ""),
                sharedPreferences.getString("login", ""),
                "Encerrado"
        );

        ocorrencias.enqueue(new Callback<TabEncerrado>() {
            @Override
            public void onResponse(Call<TabEncerrado> call, Response<TabEncerrado> response) {

                progressDialog.dismiss();

                TabEncerrado res = response.body();

                if(res.getLista().size() > 0){
                    list.clear();
                    list.addAll(res.getLista());
                    recyclerAdapter.notifyDataSetChanged();
                }else {
                    empty.setVisibility(View.VISIBLE);
                    recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }

            }
            @Override
            public void onFailure(Call<TabEncerrado> call, Throwable t) {

                progressDialog.dismiss();

                new AlertDialog.Builder(getActivity(), R.style.AppTheme)
                        .setTitle("Atenção!")
                        .setMessage(t.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();

                empty.setVisibility(View.VISIBLE);
                recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
        });
    }

    public void reabrirOcorrencia(String id, String status, final View v) {

        retrofit2.Retrofit retrofit = new Retrofit().create();

        IServices services = retrofit.create(IServices.class);

        Call<Ocorrencias> reabrir = services.reabrirOcorrencia(id, status);

        reabrir.enqueue(new Callback<Ocorrencias>() {
            @Override
            public void onResponse(Call<Ocorrencias> call, Response<Ocorrencias> response) {

                Ocorrencias reposResponse = response.body();

                if(reposResponse.getStatus()) {
                    new AlertDialog.Builder(v.getContext())
                            .setTitle("")
                            .setMessage(reposResponse.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();

                }else {
                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Atenção!")
                            .setMessage(reposResponse.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }

            }
            @Override
            public void onFailure(Call<Ocorrencias> call, Throwable t) {

                new AlertDialog.Builder(v.getContext())
                        .setTitle("Atenção!")
                        .setMessage(t.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
            }
        });

    }
}
