package com.example.weltonlima.condoservices.ocorrencias;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.weltonlima.condoservices.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderTabEncerrado extends RecyclerView.ViewHolder{

    public TextView data_encerrado;
    public TextView descricao_encerrado;
    public TextView resposta_encerrado;
    public TextView readmore_encerrado;
    public TextView status_encerrado;
    public Button btn;

    public ViewHolderTabEncerrado(@NonNull View itemView) {
        super(itemView);

        this.data_encerrado = (TextView) itemView.findViewById(R.id.dataEncerrao);
        this.descricao_encerrado = (TextView) itemView.findViewById(R.id.descricaoEncerrado);
        this.resposta_encerrado = (TextView) itemView.findViewById(R.id.respostaEncerrado);
        this.readmore_encerrado = (TextView) itemView.findViewById(R.id.readmoreEncerrado);
        this.status_encerrado = (TextView) itemView.findViewById(R.id.statusEncerrado);
        this.btn = (Button) itemView.findViewById(R.id.btnReabrir);

    }
}
