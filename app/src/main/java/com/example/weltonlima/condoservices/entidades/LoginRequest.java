package com.example.weltonlima.condoservices.entidades;

public class LoginRequest {
    final String login;
    final String senha;

    LoginRequest(String login, String senha){
        this.login = login;
        this.senha = senha;
    }
}
