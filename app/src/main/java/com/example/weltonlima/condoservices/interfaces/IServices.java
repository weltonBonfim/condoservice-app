package com.example.weltonlima.condoservices.interfaces;

import com.example.weltonlima.condoservices.entidades.AreaComum;
import com.example.weltonlima.condoservices.entidades.Login;
import com.example.weltonlima.condoservices.entidades.Messages;
import com.example.weltonlima.condoservices.entidades.Ocorrencias;
import com.example.weltonlima.condoservices.entidades.ReservaResponse;
import com.example.weltonlima.condoservices.entidades.Reservas;
import com.example.weltonlima.condoservices.entidades.TabAberto;
import com.example.weltonlima.condoservices.entidades.TabEncerrado;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface IServices {
    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @POST("user/login")
    Call<Login> login(@Body JsonObject login);

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @GET("comunicados/list/{cnpj}")
    Call<Messages> messages(@Path("cnpj") String cnpj);

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @GET("ocorrencias/list/{cnpj}/{tipo}/{login}/{status}")
    Call<TabAberto> ocorrenciasAberto(@Path("cnpj") String cnpj,
                                @Path("tipo") String tipo,
                                @Path("login") String login,
                                @Path("status") String status
    );

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @GET("ocorrencias/list/{cnpj}/{tipo}/{login}/{status}")
    Call<TabEncerrado> ocorrenciasEncerrado(@Path("cnpj") String cnpj,
                                            @Path("tipo") String tipo,
                                            @Path("login") String login,
                                            @Path("status") String status
    );

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @GET("area-comum/list-area/{cnpj}")
    Call<AreaComum> areaComum(@Path("cnpj") String cnpj);

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @POST("reservas/add")
    Call<ReservaResponse> reservar(@Body JsonObject reserva);

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @GET("reservas/list/{cnpj}")
    Call<Reservas> listaReserva(@Path("cnpj") String cnpj);

    @Headers({
            "Content-Type: application/json",
            "Authorization: 1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M"
    })
    @PUT("ocorrencias/reabrir-ocorrencia/{id}/{status}")
    Call<Ocorrencias> reabrirOcorrencia(@Path("id") String id, @Path("status") String status);
}
