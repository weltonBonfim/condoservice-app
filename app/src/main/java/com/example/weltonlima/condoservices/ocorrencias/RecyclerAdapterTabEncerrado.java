package com.example.weltonlima.condoservices.ocorrencias;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.entidades.ListTabEncerrado;
import com.example.weltonlima.condoservices.entidades.Ocorrencias;
import com.example.weltonlima.condoservices.entidades.TabEncerrado;
import com.example.weltonlima.condoservices.interfaces.IServices;
import com.example.weltonlima.condoservices.mensagens.ViewHolder;
import com.example.weltonlima.condoservices.reservas.ReservasActivity;
import com.example.weltonlima.condoservices.services.Retrofit;

import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterTabEncerrado extends RecyclerView.Adapter<ViewHolderTabEncerrado> {

    private List<ListTabEncerrado> list;
    private int cont = 0;
    private int tamanhoText;
    private Context context;

    public RecyclerAdapterTabEncerrado(List<ListTabEncerrado> list) {this.list = list;}

    @NonNull
    @Override
    public ViewHolderTabEncerrado onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ocorrencias_encerrado, parent, false);

        return new ViewHolderTabEncerrado(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderTabEncerrado holder, int position) {
        final ListTabEncerrado listTabEncerrado = list.get(position);

        holder.data_encerrado.setText(listTabEncerrado.getDataOcorrencia());
        holder.descricao_encerrado.setText(listTabEncerrado.getMensagemIn());
        holder.resposta_encerrado.setText(listTabEncerrado.getMensagemOut());
        holder.status_encerrado.setText(listTabEncerrado.getStatusOcorrencia());
        tamanhoText = holder.resposta_encerrado.length();
        holder.readmore_encerrado.setPaintFlags(holder.readmore_encerrado.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if(!(tamanhoText > 100)) {
            holder.readmore_encerrado.setVisibility(View.INVISIBLE);
        }else {
            holder.readmore_encerrado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cont == 0) {
                        holder.readmore_encerrado.setText("Leia menos");
                        holder.resposta_encerrado.setMaxLines(Integer.MAX_VALUE);
                        cont++;
                    } else {
                        holder.readmore_encerrado.setText("Leia mais");
                        holder.resposta_encerrado.setMaxLines(4);
                        cont = 0;
                    }
                }
            });
        }

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabEncerrados tab = new TabEncerrados();
                tab.reabrirOcorrencia(listTabEncerrado.getIdOcorrencia().toString(), "Em Aberto", v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
