package com.example.weltonlima.condoservices.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.example.weltonlima.condoservices.R;
import com.example.weltonlima.condoservices.services.Retrofit;
import com.example.weltonlima.condoservices.entidades.Login;
import com.example.weltonlima.condoservices.home.HomeActivity;
import com.example.weltonlima.condoservices.interfaces.IServices;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private TextInputEditText login;
    private TextInputEditText password;
    private MaterialButton btn_logar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Processando...");
        progressDialog.setCancelable(false);

        sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);

        if(sharedPreferences.getBoolean("auth", false)){
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
            finish();
        }

        login  = (TextInputEditText)findViewById(R.id.login);
        password  = (TextInputEditText)findViewById(R.id.senha);
        btn_logar = (MaterialButton)findViewById(R.id.btn_logar);

        btn_logar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                    call();
            }
        });
    }

    public void call()  {

        retrofit2.Retrofit retrofit = new Retrofit().create();

        IServices services = retrofit.create(IServices.class);

        JsonObject json = new JsonObject();
        json.addProperty("login",login.getText().toString());
        json.addProperty("senha",password.getText().toString());

        Call<Login> login = services.login(json);
        login.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                Login reposResponse = response.body();

                if(!reposResponse.getStatus()) {
                    progressDialog.dismiss();

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Atenção!")
                            .setMessage(reposResponse.getMessage())
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }else {
                    sharedPreferences = getSharedPreferences("Condoservices", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putBoolean("auth", true);
                    editor.putString("cnpj", reposResponse.getData().get(0).getCnpj());
                    editor.putString("nome", reposResponse.getData().get(0).getNome());
                    editor.putString("cpf", reposResponse.getData().get(0).getCpf());
                    editor.putString("razao_social", reposResponse.getData().get(0).getRazaoSocial());
                    editor.putString("tipo", reposResponse.getData().get(0).getTipo());
                    editor.putString("login", reposResponse.getData().get(0).getLogin());

                    editor.commit();
                    progressDialog.dismiss();
                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                progressDialog.dismiss();
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Atenção!")
                        .setMessage(t.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
            }
        });
    }
}
