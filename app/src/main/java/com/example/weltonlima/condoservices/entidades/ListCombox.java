package com.example.weltonlima.condoservices.entidades;

public class ListCombox {
    private Integer idArea;
    private String nome;

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
