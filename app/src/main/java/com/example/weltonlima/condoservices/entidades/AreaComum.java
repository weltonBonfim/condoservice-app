package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AreaComum {

    @SerializedName("lista")
    @Expose
    private List<ListAreaComum> lista = null;

    public List<ListAreaComum> getLista() {
        return lista;
    }

    public void setLista(List<ListAreaComum> lista) {
        this.lista = lista;
    }
}
