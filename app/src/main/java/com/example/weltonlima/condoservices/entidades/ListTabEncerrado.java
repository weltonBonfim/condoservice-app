package com.example.weltonlima.condoservices.entidades;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListTabEncerrado {

    @SerializedName("cnpj_condominio")
    @Expose
    private String cnpjCondominio;
    @SerializedName("data_ocorrencia")
    @Expose
    private String dataOcorrencia;
    @SerializedName("mensagem_in")
    @Expose
    private String mensagemIn;
    @SerializedName("login_usuario")
    @Expose
    private String loginUsuario;
    @SerializedName("id_ocorrencia")
    @Expose
    private Integer idOcorrencia;
    @SerializedName("titulo")
    @Expose
    private String titulo;
    @SerializedName("mensagem_out")
    @Expose
    private String mensagemOut;
    @SerializedName("status_ocorrencia")
    @Expose
    private String statusOcorrencia;

    public String getCnpjCondominio() {
        return cnpjCondominio;
    }

    public void setCnpjCondominio(String cnpjCondominio) {
        this.cnpjCondominio = cnpjCondominio;
    }

    public String getDataOcorrencia() {
        return dataOcorrencia;
    }

    public void setDataOcorrencia(String dataOcorrencia) {
        this.dataOcorrencia = dataOcorrencia;
    }

    public String getMensagemIn() {
        return mensagemIn;
    }

    public void setMensagemIn(String mensagemIn) {
        this.mensagemIn = mensagemIn;
    }

    public String getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    public Integer getIdOcorrencia() {
        return idOcorrencia;
    }

    public void setIdOcorrencia(Integer idOcorrencia) {
        this.idOcorrencia = idOcorrencia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensagemOut() {
        return mensagemOut;
    }

    public void setMensagemOut(String mensagemOut) {
        this.mensagemOut = mensagemOut;
    }

    public String getStatusOcorrencia() {
        return statusOcorrencia;
    }

    public void setStatusOcorrencia(String statusOcorrencia) {
        this.statusOcorrencia = statusOcorrencia;
    }
}
